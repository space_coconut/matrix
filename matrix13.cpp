#include <matrix13.h>

template <class T>
Matrix<T>::Matrix(size_t N, size_t M){
    Amount_string=N;
    Amount_column=M;
    data=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        data[i]=new T[Amount_column];
}

template <class T>
T&Matrix<T>::operator ()(size_t Index1, size_t Index2){
    T Null;
    if(Index1>0 && Index2>0 && Index1<=Amount_string && Index2<=Amount_column)
        return data[Index1-1][Index2-1];
    else
        return Null;
}

template <class T>
void Matrix<T>::show(){
    for(size_t i=0; i<Amount_string; i++)
    {
        for(size_t j=0; j<Amount_column; j++)
        {
            std::cout<< data[i][j]<< " ";
        }
        std::cout<< std::endl;
    }
}

template <class T>
void Matrix<T>::clear(){
    delete[]data;
    data=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        data[i]=new T[Amount_column];
}

template <class T>
void Matrix<T>::add_string(T Value){
    Amount_string++;
    T**buffer=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        buffer[i]=new T[Amount_column];
    for(size_t i=0; i<Amount_string-1; i++)
        for(size_t j=0; j<Amount_column; j++)
            buffer[i][j]=data[i][j];
    data=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        data[i]=new T[Amount_column];
    for(size_t i=0; i<Amount_string-1; i++)
        for(size_t j=0; j<Amount_column; j++)
            data[i][j]=buffer[i][j];


    for(size_t i=0; i<Amount_column; i++)
        data[Amount_string-1][i]=Value;
}

template <class T>
void Matrix<T>::add_column(T Value){
    Amount_column++;
    T**buffer=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        buffer[i]=new T[Amount_column];
    for(size_t i=0; i<Amount_string; i++)
        for(size_t j=0; j<Amount_column-1; j++)
            buffer[i][j]=data[i][j];
    data=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        data[i]=new T[Amount_column];
    for(size_t i=0; i<Amount_string; i++)
        for(size_t j=0; j<Amount_column-1; j++)
            data[i][j]=buffer[i][j];

    for(size_t i=0; i<Amount_string; i++)
        data[i][Amount_column-1]=Value;
}

template <class T>
void Matrix<T>::del_string(){
    if(Amount_string>0)
    {
        Amount_string--;
        T**buffer=new T*[Amount_string];
        for(size_t i=0; i<Amount_string; i++)
            buffer[i]=new T[Amount_column];
        for(size_t i=0; i<Amount_string; i++)
            for(size_t j=0; j<Amount_column; j++)
                buffer[i][j]=data[i][j];
        Amount_string++;
        data=new T*[Amount_string];
        for(size_t i=0; i<Amount_string; i++)
            data[i]=new T[Amount_column];
        Amount_string--;
        for(size_t i=0; i<Amount_string; i++)
            for(size_t j=0; j<Amount_column; j++)
                data[i][j]=buffer[i][j];
    }

}

template <class T>
void Matrix<T>::del_column(){
    if(Amount_column>0)
    {
        Amount_column--;
        T**buffer=new T*[Amount_string];
        for(size_t i=0; i<Amount_string; i++)
            buffer[i]=new T[Amount_column];
        for(size_t i=0; i<Amount_string; i++)
            for(size_t j=0; j<Amount_column; j++)
                buffer[i][j]=data[i][j];
        Amount_column++;
        data=new T*[Amount_string];
        for(size_t i=0; i<Amount_string; i++)
            data[i]=new T[Amount_column];
        Amount_column--;
        for(size_t i=0; i<Amount_string; i++)
            for(size_t j=0; j<Amount_column; j++)
                data[i][j]=buffer[i][j];
    }
}

template <class T>
void Matrix<T>::operator=(const Matrix<T>&matrix){
    Amount_string=matrix.Amount_string;
    Amount_column=matrix.Amount_column;
    for(size_t i=0; i<Amount_string; i++)
        delete[]data[i];
    delete[]data;
    data=new T*[Amount_string];
    for(size_t i=0; i<Amount_string; i++)
        data[i]=new T[Amount_column];
    for(size_t i=0; i<Amount_string; i++)
        for(size_t j=0; j<Amount_column; j++)
            data[i][j]=matrix.data[i][j];
}

template <class T>
void Matrix<T>::transposition(){
    if(Amount_column==Amount_string)
    {
        for(size_t i=0; i<Amount_string-1; i++)
            for(size_t j=1+(i-1); j<Amount_column; j++)
            {
                T buffer;
                buffer=data[i][j];
                data[i][j]=data[j][i];
                data[j][i]=buffer;
            }
    }
}

template <class T>
void Matrix<T>::submatrix(Matrix<T>&matrix, size_t New_Amount_string, size_t New_Amount_column){
    if(New_Amount_string<=Amount_string && New_Amount_column<=Amount_column && New_Amount_string>0 && New_Amount_column>0)
    {
        Amount_string=New_Amount_string;
        Amount_column=New_Amount_column;
        data=new T*[Amount_string];
        for(size_t i=0; i<Amount_string; i++)
        {
            data[i]=new T[Amount_column];
        }
        for(size_t i=0; i<Amount_string; i++)
            for(size_t j=0; j<Amount_column; j++)
                data[i][j]=matrix.data[i][j];
    }
    else throw "Submatrix > Matrix or < 1";
}








