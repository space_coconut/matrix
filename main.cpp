#include <iostream>
#include <matrix13.h>
#include <matrix13.cpp>
#include <time.h>

int main()
{
    srand(time(NULL));

    Matrix<int> A(3,3);





    for(size_t i=1; i<=A.Amount_string; i++)
        for(size_t j=1; j<=A.Amount_column; j++)
            A(i, j)=rand()%10;

    A.add_string(0);

    A.del_string();

    //        A.del_string();
    //            A.del_string();
    //                A.del_string();
    //                    A.del_string();

    A.add_column(1);

    A.del_column();

    //        A.del_column();
    //            A.del_column();
    //                A.del_column();
    //                    A.del_column();
    std::cout<< "Matrix:" << std::endl;

    A.show();

    std::cout<< std::endl << "Same matrix:" << std::endl;

    Matrix<int> B(4,4);
    B=A;
    B.show();

    std::cout<< std::endl << "Transposition matrix:" <<std::endl;

    A.transposition();

    A.show();

    std::cout<< std::endl << "Submatrix:" << std::endl;


    Matrix<int> C(3,3);
    C.submatrix(B, 2,2);

    C.show();

    return 0;
}








