#ifndef MATRIX13_H
#define MATRIX13_H

template <class T>
class Matrix{
public:
    size_t Amount_string, Amount_column;
    T**data;

    Matrix(size_t N, size_t M);

    T&operator() (size_t Index1, size_t Index2);

    void show();

    void clear();

    void add_string(T Value);

    void add_column(T Value);

    void del_string();

    void del_column();

    void operator=(const Matrix<T>&matrix);

    void transposition();

    void submatrix(Matrix<T>&matrix, size_t New_Amount_string, size_t New_Amount_column);
};

#endif // MATRIX13_H
